# Template card partida
----

## Intruções:

1. Copiar o código CSS e colar no cabeçalho da página.
2. Utilizar o template no *for each* das partidas;

----
## Exemplo de uso
![print exemplo](https://i.ibb.co/9cRnXcR/PRINT.png)
---
## Acesso rápido ao template:
### CSS
---
```html
<style type="text/css">
    .hlz-is-align-center {
        -ms-flex-align: center;
        align-items: center;
    }
    .hlz-mb-0 {
        margin-bottom: 0 !important;
    }
    .hlz-card-padding {
        padding: 10px 20px;
    }
    .hlz-is-flex-column {
        flex-direction: column;
    }
    .hlz-is-justify-center {
        justify-content: center;
    }
    .hlz-decoration-none {
        text-decoration: none !important;
    }
    .hlz-placar-padding {
        padding: 0px 5px;
    }
    .hlz-card-time {
        min-width: 100px;
        max-width: 100px;
    }
</style>
```
---
### HTML
---

```html
<div class="media box hlz-mb-0 hlz-card-padding hlz-is-align-center">
    <!-- TIME #1 -->
    <a href="#linkteam" onclick="/*ou evento js*/" title="time 1" class="media-left is-flex has-text-black hlz-is-align-center hlz-is-justify-center hlz-is-flex-column hlz-card-time hlz-decoration-none">
        <!-- Brasão do time 1 -->
        <div class="image is-48x48">
            <img src="https://s.glbimg.com/es/sde/f/equipes/2016/05/03/inter45.png">
        </div>
        <!-- Nome do time 1 -->
        <p class="is-size-7 has-text-centered">INTERNACIONAL</p>
    </a>
    <!-- Placar -->
    <div class="media-content">
        <!-- Link ou instrução javascript pra partida -->
        <a href="#link-partida" onclick="/*ou instrução js pra modal...*/" class="has-text-black hlz-decoration-none" title="Ver partida">
            <div>
                <p class="is-marginless has-text-centered is-size-4">
                    <!-- Placar Time 1 -->
                    <strong>2</strong>

                    <span class="hlz-placar-padding">x</span>

                    <!-- Placar Time 2 -->
                    <strong>2</strong>
                </p>
                <!-- /Placar -->
            </div>
        </a>
    </div>
    <!-- TIME #2 -->
    <a href="#linkteam" onclick="/*ou evento js*/" title="time 2" class="media-right is-flex has-text-black hlz-is-align-center hlz-is-justify-center hlz-is-flex-column hlz-card-time hlz-decoration-none">
        <!-- Brasão do time 2 -->
        <div class="image is-48x48">
            <img src="https://s.glbimg.com/es/sde/f/equipes/2013/12/16/gremio_45x45.png">
        </div>
        <!-- Nome do time 2 -->
        <p class="is-size-7 has-text-centered">GRÊMIO</p>
    </a>
</div>
```
---